﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    const int LEFT_MOUSE_BUTTON = 0;
    const int RIGHT_MOUSE_BUTTON = 1;
    const string Q_BUTTON = "q";
    const string R_BUTTON = "r";

    private NavMeshAgent navMeshAgent;
    private float maxPlayerHelth = 100f;
    private GameObject targetObj;
    private float clicks = 0.0f;
    private bool isEquiped = false;
    private Vector3 destPoint = new Vector3(0, 0, 0);
    private bool isDead = false;

    public Animator _Animator;
    public Texture2D HelthTexture;
    public float Multiplier;
    public float CurrentPlayerHelth = 100f;
    public float Damage = 15f;
    public float AttackDistance = 1.5f;
    public GameObject NPCBloodPS;
    public static bool InventoryBusy;
    public float DampTime = 0.5f;
    public bool IsUnderAttack = false;

    public Transform Weapon;
    public Transform Shield;
    public Transform Righthandpos;
    public Transform Lefthandpos;
    public Transform Chestholder;
    public Transform Chestholdersword;

    void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        InventoryBusy = false;
        _Animator = this.GetComponentInChildren<Animator>();
    }

    IEnumerator Equip()
    {
        _Animator.CrossFade("overlordequip", 0.0f, 0, 0.0f);
        yield return new WaitForSeconds(0.6f);
        Weapon.parent = Righthandpos;
        Weapon.position = Righthandpos.position;
        Weapon.rotation = Righthandpos.rotation;
        Shield.parent = Lefthandpos;
        Shield.position = Lefthandpos.position;
        Shield.rotation = Lefthandpos.rotation;
        _Animator.SetBool("isequip", true);
        _Animator.SetBool("equip_key_presed", false);
    }

    IEnumerator Holsten()
    {
        _Animator.CrossFade("overlordequip", 0.0f, 0, 0.0f);
        yield return new WaitForSeconds(0.6f);
        Weapon.parent = Chestholdersword;
        Weapon.position = Chestholdersword.position;
        Weapon.rotation = Chestholdersword.rotation;
        Shield.parent = Chestholder;
        Shield.position = Chestholder.position;
        Shield.rotation = Chestholder.rotation;
        _Animator.SetBool("isequip", false);
        _Animator.SetBool("equip_key_presed", false);
    }

    void OnGUI()
    {
        if ((CurrentPlayerHelth > 0f) && (CurrentPlayerHelth <= 100f))
        {
            Multiplier = CurrentPlayerHelth / maxPlayerHelth;
            GUI.DrawTexture(new Rect(Screen.width - 300, Screen.height - 100, 250 * Multiplier, 50), HelthTexture);
        }
        else
        {
            GUI.Box(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 150, 300, 300), "");
            GUI.BeginGroup(new Rect(Screen.width / 2 - 200, Screen.height / 2 - 150, 600, 300));
            GUI.Label(new Rect(175, 100, 100, 50), "Game Over");
            if (GUI.Button(new Rect(130, 150, 150, 50), "Back to Main Menu"))
            {
                //    Application.LoadLevel ("mainMenu");
            }
            GUI.EndGroup();
        }
        if (Input.GetKeyDown(Q_BUTTON))
        {
            //   Application.LoadLevel("mainMenu");
        }
    }

    IEnumerator Attack(GameObject targetObj)
    {
        Destroy(GameObject.FindWithTag("BloodNPC"));
        targetObj.GetComponent<BotAI>().IsUnderAttack = true;
        yield return new WaitForSeconds(0.3f);
        targetObj.GetComponent<BotAI>()._Animator.CrossFade("monster1WeaponHit1", 0.0f, 0, 0.0f);
        targetObj.GetComponent<BotAI>().NPCHealth = (targetObj.GetComponent<BotAI>().NPCHealth - Damage);
        Instantiate(NPCBloodPS, targetObj.transform.position, targetObj.transform.rotation);
        yield return new WaitForSeconds(0.3f);
        targetObj.GetComponent<BotAI>().IsUnderAttack = false;
        if (clicks <= 3.0f)
        {
            clicks += 1.0f;
        }
        else
        {
            clicks = 1.0f;
        }
    }

    void Update()
    {
        if (CurrentPlayerHelth <= 0 && !isDead)
        {
            _Animator.CrossFade("overlorddeath", 0.0f, 0, 0.0f);
            isDead = true;
        }
        if (Input.GetKeyDown(R_BUTTON) && CurrentPlayerHelth > 0f)
        {
            if (!isEquiped)
            {
                StartCoroutine(Equip());
                _Animator.SetBool("equip_key_presed", true);
            }
            else
            {
                StartCoroutine(Holsten());
                _Animator.SetBool("equip_key_presed", true);
            }
            isEquiped = !isEquiped;
        }

        _Animator.SetBool("atack", false);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Input.GetMouseButtonDown(LEFT_MOUSE_BUTTON) && !InventoryBusy && CurrentPlayerHelth > 0f && !IsUnderAttack)
        {
            if (Physics.Raycast(ray, out hit, 100) && (hit.collider.tag == "Enemy") && ((Vector3.Distance(transform.position, hit.collider.gameObject.transform.position) <= AttackDistance)) && !hit.collider.gameObject.GetComponent<BotAI>().IsDead)
            {
                transform.LookAt(hit.collider.transform);
                _Animator.SetBool("atack", true);
                _Animator.SetFloat("click`s", clicks, 0.0f, 0.0f);
                _Animator.SetBool("run", false);
                StartCoroutine(Attack(hit.collider.gameObject));
            }
            else
                if (Physics.Raycast(ray, out hit, 100))
                {
                    if (transform.position != hit.transform.position)
                    {
                        destPoint = hit.point;
                        _Animator.SetBool("atack", false);
                        _Animator.SetBool("run", true);
                        navMeshAgent.destination = hit.point;
                        navMeshAgent.Resume();
                    }
                }
        }
        else if (destPoint != new Vector3(0, 0, 0) && Vector3.Distance(transform.position, destPoint) < 1f && CurrentPlayerHelth > 0f)
        {
            _Animator.SetBool("run", false);
           // _Animator.SetBool("idle", true);
        }
    }
}
