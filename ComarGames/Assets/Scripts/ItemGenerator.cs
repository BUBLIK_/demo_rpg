﻿using UnityEngine;
using System.Collections;

public class ItemGenerator  : MonoBehaviour {

    public static ItemGenerator _ItemGenerator;
    public GameObject Axe;
    public GameObject Armor;
    public GameObject Shield;

    void Awake()
    {
        _ItemGenerator = this;
    }

    public void Generete(Vector3 PosForInstance,Quaternion RotForInstance)
    {
        int index = Random.Range(0, 3);
        switch(index)
        {
            case 0:
                Instantiate(Axe, PosForInstance, RotForInstance);
                break;
            case 1:
                Instantiate(Armor, PosForInstance, RotForInstance);
                break;
            case 2:
                Instantiate(Shield, PosForInstance, RotForInstance);
                break;
        }
    }
}
