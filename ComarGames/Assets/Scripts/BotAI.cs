﻿using UnityEngine;
using System.Collections;

public class BotAI : MonoBehaviour
{
    public float Speed = 5f;
    public float Damage = 5f;
    public float NPCHealth = 100f;
    public float AttackDistance = 1.5f;
    public float TimeAttack = 1f;
    public float DeathDampTime = 1.5f;
    public bool Delay = false;
    public bool CanAttack = true;
    public bool IsUnderAttack = false;
    public GameObject BloodPS;
    public bool IsDead = false;

    public Transform Weapon;
    public Transform Shield;
    public Transform Righthandpos;
    public Transform Lefthandpos;
    public Transform Chestholder;
    public Transform Chestholdersword;
    public Animator _Animator;

    private NavMeshAgent navMeshAgent;
    private Transform target;
    private float attacks = 0.0f;

    void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        _Animator = this.GetComponentInChildren<Animator>();
    }

    void Start()
    {
        target = GameObject.FindWithTag("Player").transform;
        StartCoroutine(Equip());
    }

    IEnumerator Equip()
    {
        _Animator.CrossFade("monster1Grabweapon", 0.0f, 0, 0.0f);
        yield return new WaitForSeconds(0.6f);
        Weapon.parent = Righthandpos;
        Weapon.position = Righthandpos.position;
        Weapon.rotation = Righthandpos.rotation;
        Shield.parent = Lefthandpos;
        Shield.position = Lefthandpos.position;
        Shield.rotation = Lefthandpos.rotation;
    }

    IEnumerator Holsten()
    {
        _Animator.CrossFade("monster1Grabweapon", 0.0f, 0, 0.0f);
        yield return new WaitForSeconds(0.6f);
        Weapon.parent = Chestholdersword;
        Weapon.position = Chestholdersword.position;
        Weapon.rotation = Chestholdersword.rotation;
        Shield.parent = Chestholder;
        Shield.position = Chestholder.position;
        Shield.rotation = Chestholder.rotation;
    }

    IEnumerator AttackSetDelay()
    {
        CanAttack = false;
        yield return new WaitForSeconds(0.3f);
        _Animator.SetBool("isAttack", false);
        Delay = true;
    }

    void Update()
    {      
        if (NPCHealth < 0)
        {
            Death();
        }
        if (Vector3.Distance(transform.position, target.transform.position) <= AttackDistance && !IsUnderAttack && !IsDead)
        {
            _Animator.SetBool("isRuning", false);
            navMeshAgent.SetDestination(transform.position);
            transform.LookAt(target);
            #region Attack
            if (target.GetComponent<PlayerController>().CurrentPlayerHelth > 0f)
            {
                if (TimeAttack >= 1.5 && TimeAttack <= 1.7)
                {
                    Destroy(GameObject.FindWithTag("Blood"));
                    _Animator.SetBool("isAttack", false);
                }
                if (TimeAttack >= 2 && TimeAttack <= 2.5)
                {
                    target.GetComponent<PlayerController>().IsUnderAttack = false;
                }
                if (TimeAttack >= 0)
                {
                    TimeAttack -= Time.deltaTime;
                }
                else
                {
                    if (attacks < 3f)
                    {
                        if (CanAttack && !Delay)
                        {
                            attacks++;
                            _Animator.SetBool("isAttack", true);
                            _Animator.SetFloat("AttackTreeBlend", attacks, 0.0f, 0.0f);
                            StartCoroutine(AttackSetDelay());
                        }
                        if (Delay)
                        {
                            target.GetComponent<PlayerController>().CurrentPlayerHelth = (target.GetComponent<PlayerController>().CurrentPlayerHelth - Damage);
                            TimeAttack = 3f;
                            Instantiate(BloodPS, target.transform.position, target.transform.rotation);
                            target.GetComponent<PlayerController>()._Animator.CrossFade("attackgethit", 0.0f, 0, 0.0f);
                            Delay = false;
                            CanAttack = true;
                            target.GetComponent<PlayerController>().IsUnderAttack = true;
                        }
                    }
                    else
                    {
                        attacks = 0.0f;
                    }
                }
            }
            else
            {
                StartCoroutine(Holsten());
            }
            #endregion
        }
        else if (!IsUnderAttack && !IsDead)
        {
            _Animator.SetBool("isAttack", false);
            navMeshAgent.speed = Speed;
            navMeshAgent.SetDestination(target.transform.position);
            _Animator.SetBool("isRuning", true);
        }
    }

    void Death()
    {
        if (!IsDead)
        {
            _Animator.CrossFade("monster1Die", 0.0f, 0, 0.0f);
            IsDead = true;
        }
        if (DeathDampTime >= 0)
        {
            DeathDampTime -= Time.deltaTime;
        }
        else
        {
            ItemGenerator._ItemGenerator.Generete(transform.position, transform.rotation);
            GameObject.Destroy(this.gameObject);
        }
    }
}

