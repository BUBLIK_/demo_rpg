﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Inventory_DD : MonoBehaviour
{
    const int RIGHT_MOUSE_BTN = 1;
    List<Item_DD> inventory_List;

    public GameObject Inventory_GUI;
    public GameObject Container;
  
    void Start()
    {
        inventory_List = new List<Item_DD>();
    }

    void Update()
    {
        if (Input.GetMouseButtonUp(RIGHT_MOUSE_BTN))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                Item_DD item = hit.collider.GetComponent<Item_DD>();
                if (item != null)
                {
                    inventory_List.Add(item);
                    Destroy(hit.collider.gameObject);
                }
            }
        }

        if (Input.GetKeyUp(KeyCode.I))
        {
            if (Inventory_GUI.activeSelf)
            {
                Inventory_GUI.SetActive(false);
                for (int i = 0; i < Inventory_GUI.transform.childCount; i++)
                {
                    Destroy(Inventory_GUI.transform.GetChild(i).transform.GetChild(0).gameObject);
                }
            }
            else
            {
                Inventory_GUI.SetActive(true);
                for (int i = 0; i < inventory_List.Count; i++)
                {
                    Item_DD item = inventory_List[i];
                    if (Inventory_GUI.transform.childCount >= i)
                    {
                        GameObject img = Instantiate(Container);
                        img.transform.SetParent(Inventory_GUI.transform.GetChild(i).transform);
                        img.GetComponent<Image>().sprite = Resources.Load<Sprite>(item.icon);
                        img.GetComponent<Drag>().item = item;
                    }
                    else break;
                }
            }
        }
    }

    public void Remove(Drag drag)
    {
        Item_DD item = drag.item;
        GameObject new_obj = Instantiate<GameObject>(Resources.Load<GameObject>(item.prefab));
        new_obj.transform.position = transform.position + transform.forward + transform.up;
        Destroy(drag.gameObject);
        inventory_List.Remove(item);
    }
}
