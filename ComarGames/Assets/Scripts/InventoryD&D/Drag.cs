﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Drag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler
{
    public Transform Canvas;
    public Transform Old;
    public Item_DD item;
    public GameObject Player;

	void Start () 
    {
        Canvas = GameObject.Find("Canvas").transform;
        Player = GameObject.FindWithTag("Player");
	}
	
    public void OnBeginDrag(PointerEventData eventData)
    {
        Old = transform.parent;
        transform.SetParent(Canvas);
        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        if (transform.parent == Canvas)
        {
            transform.SetParent(Old);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Player.GetComponent<Inventory_DD>().Remove(this);
    }
}
