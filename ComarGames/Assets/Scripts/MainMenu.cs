﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class MainMenu : MonoBehaviour
{
    //float window = 0;
    //float volumeValue;
    public GameObject Options;
    public GameObject _MainMenu;
    public GameObject Start_Img;
    public GameObject Options_Img;
    public GameObject Exit_Img;
   
    public void Start_Btn_Click()
    {
        //  Application.LoadLevel("Game");   
    }

    public void Exit_Btn_Click()
    {
        //  Application.Quit();   
    }

    public void Options_Btn_Click()
    {
        Options.SetActive(!Options.activeSelf);
        _MainMenu.SetActive(!_MainMenu.activeSelf);
    }

    public void Pointer_Enter_Sart_Btn()
    {
        Start_Img.SetActive(!Start_Img.activeSelf);
    }

    public void Pointer_Exit_Sart_Btn()
    {
        Start_Img.SetActive(!Start_Img.activeSelf);
    }

    public void Pointer_Enter_Options_Btn()
    {
        Options_Img.SetActive(true);
    }

    public void Pointer_Exit_Options_Btn()
    {
        Options_Img.SetActive(false);
    }

    public void Pointer_Enter_Exit_Btn()
    {
        Exit_Img.SetActive(!Exit_Img.activeSelf);
    }

    public void Pointer_Exit_Exit_Btn()
    {
        Exit_Img.SetActive(!Exit_Img.activeSelf);
    }
    
/*
    void OnGUI()
    {
        if (window == 0)
        {
            if (GUI.Button(new Rect(Screen.width / 2 - 75, Screen.height / 2 - 100, 150, 50), "Start"))
            {
               //  Application.LoadLevel("Game");
            }
            if (GUI.Button(new Rect(Screen.width / 2 - 75, Screen.height / 2 - 25, 150, 50), "Options"))
            {
                window = 1;
            }
            if (GUI.Button(new Rect(Screen.width / 2 - 75, Screen.height / 2 + 50, 150, 50), "Quit"))
            {
                Application.Quit();
            }
        }
        if (window == 1)
        {
            GUI.Box(new Rect(Screen.width / 2 - 200, Screen.height / 2 - 150, 400, 300), "Opltions");
            GUI.BeginGroup(new Rect(Screen.width / 2 - 200, Screen.height / 2 - 150, 400, 300));
            GUI.Label(new Rect(50, 100, 150, 50), "Volume");
            if (GUI.Button(new Rect(125, 200, 150, 50), "Back"))
            {
                window = 0;
                AudioListener.volume = volumeValue;
            }
            volumeValue = GUI.HorizontalSlider(new Rect(50 , Screen.height / 2 - 25, 300, 100), volumeValue, 0.01f, 1.0f);
            GUI.EndGroup();
        }
    }*/
}
