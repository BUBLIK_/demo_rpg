﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public Transform Target;
    public float Smooth = 5f;
    Vector3 offset;

    void Start()
    {
        offset = transform.position - Target.position;
    }

    void Update()
    {
        Vector3 targetCameraPosition = Target.position + offset;
        transform.position = Vector3.Lerp(transform.position, targetCameraPosition, Smooth * Time.deltaTime);
    }
}
