﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Item
{
    public Texture2D Texture; 
    public string Name;  
}