﻿using UnityEngine;
using System.Collections;

public  class ItemInv : MonoBehaviour 
{
    public static ItemInv _ItemInv;
    public Texture2D ItemTexture;
    public string ItemName;

    void Awake()
    {
        _ItemInv = this;
    } 

    public  Item ReturnItem()
    {     
        Item InvItem = new Item();
        InvItem.Name = ItemName;
        InvItem.Texture = ItemTexture;
        return InvItem;
    }
}
