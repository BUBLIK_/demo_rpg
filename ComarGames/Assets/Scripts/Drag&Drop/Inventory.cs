﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour {

    const int RIGHT_MOUSE_BUTTON = 1;
    const string INVENTORY_BUTTON = "i";
    const int INVENTORY_WINDOW_ID = 0;
    const int INVENTORY_TEXTURE_ID = 1;
    public float ButtonWidth = 40;
    public float ButtonHeight = 40;
    public float PickUpDistance = 2f;

    bool showInventory = false;
    int invRows = 3;
    int invColumns = 3;
    public Rect inventoryWindowRect;
    Rect inventoryBoxRect = new Rect();
    bool isDraggable;
    Item selectItem;
    Texture2D dragTexture;

    public Dictionary<int, Item> InventoryPlayer = new Dictionary<int, Item>();

    void Start()
    {
        inventoryWindowRect = new Rect(10, 10, ButtonWidth * invColumns + 10, ButtonHeight * invRows + 25);
    }

    void OnGUI()
    {
        if (showInventory)
        {
            DetectGUIAction();
            inventoryWindowRect = GUI.Window(INVENTORY_WINDOW_ID, inventoryWindowRect, FirstInventory, "INVENTORY");
            if (isDraggable)
            {
                inventoryBoxRect = GUI.Window(INVENTORY_TEXTURE_ID, new Rect(Event.current.mousePosition.x + 1, Event.current.mousePosition.y + 1, 40, 40), Insert, "", "box");

            }
        }
    }

    void Insert(int id)
    {
        GUI.BringWindowToFront(INVENTORY_TEXTURE_ID);
        GUI.DrawTexture(new Rect(Event.current.mousePosition.x, Event.current.mousePosition.y, 40, 40), dragTexture);
    }

    void DetectGUIAction()
    {
        if ((Input.mousePosition.x > inventoryWindowRect.x && Input.mousePosition.x < inventoryWindowRect.x + inventoryWindowRect.width) && (Screen.height - Input.mousePosition.y > inventoryWindowRect.y && Screen.height - Input.mousePosition.y < inventoryWindowRect.y + inventoryWindowRect.height))
        {
            PlayerController.InventoryBusy = true;
            return;
        }
        PlayerController.InventoryBusy = false;
    }

    void FirstInventory(int id)
    {
        for (int y = 0; y < invRows; y++)
        {
            for (int x = 0; x < invColumns; x++)
            {
                if (InventoryPlayer.ContainsKey(x + y * invColumns))
                {
                    if ((GUI.Button(new Rect(5 + (x * ButtonHeight), 20 + (y * ButtonHeight), ButtonWidth, ButtonHeight), new GUIContent(InventoryPlayer[x + y * invColumns].Texture), "button")) && (!isDraggable))
                    {
                        dragTexture = InventoryPlayer[x + y * invColumns].Texture;
                        isDraggable = true;
                        selectItem = InventoryPlayer[x + y * invColumns];
                        InventoryPlayer.Remove(x + y * invColumns);
                    }
                }
                else
                {
                    if (isDraggable)
                    {
                        if (GUI.Button(new Rect(5 + (x * ButtonHeight), 20 + (y * ButtonHeight), ButtonWidth, ButtonHeight), "", "button"))
                        {
                            InventoryPlayer.Add(x + y * invColumns, selectItem);
                            isDraggable = false;
                            selectItem = null;
                        }
                    }
                    else
                    {
                        GUI.Label(new Rect(5 + (x * ButtonHeight), 20 + (y * ButtonHeight), ButtonWidth, ButtonHeight), "", "button");
                    }
                }
            }
        }
        GUI.DragWindow();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(INVENTORY_BUTTON))
        {
            showInventory = !showInventory;
        }

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Input.GetMouseButtonDown(RIGHT_MOUSE_BUTTON))
        {
            if (Physics.Raycast(ray, out hit, 100) && hit.collider.CompareTag("Item") && ((Vector3.Distance(transform.position, hit.collider.gameObject.transform.position) <= PickUpDistance)))
            {
                InventoryPlayer.Add(InventoryPlayer.Count, hit.collider.GetComponent<ItemInv>().ReturnItem());
                GameObject.Destroy(hit.collider.gameObject);
            }
        }
    }
}
